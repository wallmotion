#!/bin/bash

for i in video_file_missing video_file_error; do
	ffmpeg -stream_loop 16 -i $i.png -vc libx264 -pix_fmt yuv420p -vprofile baseline -vf setpts=N/TB -movflags +faststart $i.mp4
done


