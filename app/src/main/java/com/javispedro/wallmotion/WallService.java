package com.javispedro.wallmotion;

import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.SurfaceHolder;

public class WallService extends WallpaperService {
    private static final String TAG = "WallService";

    @Override
    public Engine onCreateEngine() {
        return new WallpaperEngine();
    }

    private class WallpaperEngine extends Engine {
        private static final String TAG = "WallpaperEngine";

        private Renderer renderer;

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            Log.d(TAG, "onCreate");
            renderer = new Renderer(WallService.this);
        }

        @Override
        public void onDestroy() {
            Log.d(TAG, "onDestroy");
            renderer.stop();
            renderer = null;
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            Log.d(TAG, "onSurfaceDestroyed");
            renderer.stop();
        }

        @Override
        public void onSurfaceCreated(SurfaceHolder holder)  {
            Log.d(TAG, "onSurfaceCreated");
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format,
                                     int width, int height) {
            Log.d(TAG, "onSurfaceChanged");
        }

        @Override
        public void onSurfaceRedrawNeeded(SurfaceHolder holder) {
            Log.d(TAG, "onSurfaceRedrawNeeded");
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            Log.d(TAG, "onVisibilityChanged(visible: " + visible + ")");
            if (visible) {
                renderer.start(getSurfaceHolder().getSurface());
            } else {
                renderer.stop();
            }
        }
    }
}
